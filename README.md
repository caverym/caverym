# Avery Murray

Linux only; No JavaScript; FOSS.

Visit my website [here](https://caverym.net/)



I know: C and Rust. I'm not a professional, probably less than an amateur, but seriously love making things and working with people.



- Photography: [Photography,](https://unsplash.com/@caverym)
- Software: [Programming](https://github.com/caverym)
- Music: [Music](https://caverym.bandcamp.com/)
- Matrix [\@caverym:envs.net](https://matrix.to/#/@caverym:envs.net)
- Discord: [Avery~#1845](https://discord.gg/Ef4pG66h7M),


[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Z8Z73VCC4)


[![Avery's GitHub stats](https://github-readme-stats.vercel.app/api?username=caverym&show_icons=true)](https://github.com/anuraghazra/github-readme-stats)

# Current projects:
## [Proton Caller](https://github.com/caverym/proton-caller)
A program to launch any given Windows executable through [Valve's Proton](https://github.com/ValveSoftware/Proton)

## [Flario](https://github.com/caverym/flario)
An actual kernel written in Rust, with a shell and a work-in-progrss temporary btree filesystem.

## [Plage](https://github.com/caverym/plage)
A very simple [AUR](https://aur.archlinux.org) helper.

## [Cimplefetch](https://github.com/caverym/cimplefetch) / [Rfetch](https://github.com/caverym/rfetch)
A simple C system info fetch program. /  A simple Rust system info fetch program.
